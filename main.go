package main

import (
	"github.com/gin-gonic/gin"
	 siam "gitlab.com/kevin.harnanta/siam-scraper"
	"log"
	"net/http"
)

func init (){
	gin.SetMode(gin.ReleaseMode)
}

func main() {
	r := gin.Default()
	r.GET("/_/healthz", func(c *gin.Context) {
		c.JSON(200, gin.H{
			"Status": 200,
			"message": "I'm Healthy",
		})
	})
	v1 := r.Group("/api/v1")
	{
		v1.POST("/rekaphasilstudi", siamScraper)
	}
	r.Run()
}

func siamScraper(c *gin.Context){
	data:= make(map[string]string)
	if c.PostForm("nim")!="" || c.PostForm("password")!="" {
		data["username"]=c.PostForm("nim")
		data["password"]=c.PostForm("password")
		data["login"]="Masuk"
		scrapping:=siam.Init()
		err:=siam.LoginSiam(scrapping,data)
		checkErr(err)
		json,err:=siam.GetRekap(scrapping)
		checkErr(err)
		c.Data(http.StatusOK,gin.MIMEJSON,json)
	}else{
		c.JSON(401, gin.H{
			"status": 401,
			"message": "You are unauthorized",
		})
	}
}

func checkErr(err error) {
	if err!=nil {
		log.Fatal(err)
	}
}